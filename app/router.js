import EmberRouter from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

 //This is were the routes for the app are set up
 //Whenver 'ember generate route' is applied THIS gets updates
Router.map(function() {
  this.route('game', {path: '/'});//using '/' dedicatres this as the initial route
});

export default Router;
